package com.rubrica.demo.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonManagedReference;
import org.hibernate.validator.constraints.Length;

import io.swagger.annotations.ApiModelProperty; 

@Entity
@Table(name="users") 
@XmlRootElement
public class User {
	//essite date come tipo      public Date dateCreation  nella lambada puoi mettergli after e before
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private Integer id;
	
	@NotBlank(message = "Campo richiesto")
	@Length(min = 2,message = "minimo 2 caratteri")
	@Column(name="name",nullable=false)
	@ApiModelProperty(notes = "nome dell'articolo")
	private String name;

	@Column(name="surname",nullable=false)
	private String surname; 

	@Column(name="image",nullable=false)
	private String image; 

	@Column(name="email",nullable=false)
	private String email;
	
	@Transient
	private String nominativo;
	

	@OneToOne( fetch = FetchType.LAZY,  mappedBy = "user",orphanRemoval = true, cascade = CascadeType.ALL) //questo user è il nome che hai in user è molto semplice usando le convenzioni di Laravel
	@JsonManagedReference
	private UserDetail detail; //one to one
	
	@OneToMany(fetch = FetchType.LAZY,cascade = CascadeType.ALL,mappedBy="user", orphanRemoval = true)
	@JsonManagedReference
	private List<Telephone> numbers; //one to many
	
	@ManyToMany(cascade = CascadeType.DETACH)
	@JoinTable
	(
		name="user_helpcenter", 
		joinColumns = {@JoinColumn(name="user_id", referencedColumnName="id")},  
		inverseJoinColumns = {@JoinColumn(name="helpcenter_id", referencedColumnName="id")}
	)
	private List<HelpCenter> helpCenters; //many to many
	
	
	public User() 
	{

	}
	
	
	public Integer getId()
	{
		return id;
	}
	public void setId(Integer id) 
	{
		this.id = id;
	}
	
	
	public String getName() 
	{
		return name;
	}
	public void setName(String name) 
	{
		this.name = name;
	}
	
	
	public String getSurname() 
	{
		return surname;
	}
	public void setSurname(String surname) 
	{
		this.surname = surname;
	}
	


	public String getNominativo() 
	{
		return this.name + " " + this.surname;
	}
	public void setNominativo(String nominativo) 
	{
		this.nominativo = nominativo;
	}
	

	public String getImage() 
	{
		return image;
	}
	public void setImage(String image) 
	{
		this.image = image;
	}
	
	public String getEmail() 
	{
		return email;
	}
	public void setEmail(String email) 
	{
		this.email = email;
	}
	
	
	public List<Telephone> getNumbers()
	{
		return numbers;
	}
	public void setNumbers(List<Telephone> numbers) 
	{
		this.numbers = numbers;
	}
	
	
	public UserDetail getDetail() 
	{
		return detail;
	}
	public void setDetail(UserDetail detail)
	{
		this.detail = detail;
	}
	
	
	public List<HelpCenter> getHelpCenters() 
	{
		return helpCenters;
	}
	public void setHelpCenters(List<HelpCenter> helpCenters) 
	{
		this.helpCenters = helpCenters;
	}
	
	
	
}
