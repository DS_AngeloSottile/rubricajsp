package com.rubrica.demo.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason ="Pagina non esiste")
public class NotFoundException  extends RuntimeException{

	private static final long serialVersionUID = -6906560787116168193L;

	
	private String code;


	public String getCode()
	{
		return code;
	}
	public void setCode(String code) 
	{
		this.code = code;
	}
	
}
