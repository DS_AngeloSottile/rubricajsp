package com.rubrica.demo.service;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.view.RedirectView;

import com.rubrica.demo.model.User;
import com.rubrica.demo.model.UserDetail;
import com.rubrica.demo.repository.UserDetailRepositoryInterface;
import com.rubrica.demo.repository.UserRepositoryInterface;

@Service("mainUserService")
@Transactional
@CacheConfig(cacheNames = {"users"})
public class UserServiceImpl implements UserServiceInterface{
	
	@Autowired
	private UserRepositoryInterface userRepository;
	
	@Autowired
	private UserDetailRepositoryInterface userDetailRepository;

	@Override
	@Transactional
	@Cacheable
	public Iterable<User> getAll() 
	{
		return userRepository.findAll();
	}
	
	@Override
	@Transactional
	public Iterable<User> getByName(String name) 
	{
		return userRepository.findUserByName(name);
	}

	@Override
	@Transactional
	@Cacheable(value = "user",key = "#id",sync = true)
	public Optional<User> getById(int id) 
	{
		return userRepository.findById(id);
	}

	@Override
	@Transactional
	@Caching( evict = {
			@CacheEvict(cacheNames = "users", allEntries = true),
			@CacheEvict(cacheNames = "user", key = "#user.id")
			})
	public User store(User user) 
	{		
		return userRepository.save(user);
	}

	@Override
	@Transactional
	public Optional<User> update(int id, User user) 
	{
		Optional<User> foundUser = userRepository.findById(id);
		if(foundUser.isEmpty())
		{
			throw new ResponseStatusException(HttpStatus.NOT_FOUND,"problem with update");
		}
		
		
		foundUser.get().setName(user.getName());
		foundUser.get().setSurname(user.getSurname());
		
		if(foundUser.get().getDetail() == null)
		{
			UserDetail userDetail = new UserDetail();
			userDetail.setUser(foundUser.get());
			userDetail.setAge(user.getDetail().getAge());
			
			userDetailRepository.save(userDetail);
			
		}
		else
		{
			foundUser.get().getDetail().setAge(user.getDetail().getAge());
		}
		
		
		foundUser.get().setHelpCenters(user.getHelpCenters());
		

		userRepository.save(foundUser.get());
		
		return foundUser;
			
	}

	@Override
	@Transactional
	public Boolean delete(int id) 
	{
		Optional<User> foundUser = userRepository.findById(id);
		
		if(foundUser.isEmpty())
		{
			return false;
		}
		userRepository.delete(foundUser.get());
		
		return true;
	}
	
	
}
