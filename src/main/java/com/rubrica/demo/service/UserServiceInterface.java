package com.rubrica.demo.service;

import java.util.Optional;

import com.rubrica.demo.model.User;

public interface UserServiceInterface {

	//get all
	public Iterable<User> getAll();
	
	//get byName
	public Iterable<User> getByName(String name);
	
	//get by id
	public Optional<User> getById( int id);
	
	//create photo
	public User store(User user);
	
	//update photo
	public Optional<User> update( int id, User user);
	
	//delete
	public Boolean delete( int id);
}
