package com.rubrica.demo.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.rubrica.demo.model.HelpCenter;

@Repository
public interface HelpCenterRepositoryInterface extends CrudRepository<HelpCenter, Integer> {

	
}
