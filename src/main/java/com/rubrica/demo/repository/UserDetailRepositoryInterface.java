package com.rubrica.demo.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.rubrica.demo.model.UserDetail;

@Repository
public interface UserDetailRepositoryInterface  extends CrudRepository<UserDetail, Integer> {

}
