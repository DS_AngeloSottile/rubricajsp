package com.rubrica.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.rubrica.demo.model.User;

@Repository
public interface UserRepositoryInterface extends JpaRepository<User, Integer> {

	public Iterable<User> findUserByName(String name);
}
