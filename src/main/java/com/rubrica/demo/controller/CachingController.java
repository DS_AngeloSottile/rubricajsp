package com.rubrica.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.web.bind.annotation.GetMapping;

public class CachingController {

	@Autowired
	CacheManager cacheManager;
	
	@GetMapping("clearAllCaches")
	public void clearAllCachies()
	{
		cacheManager.getCache("users").clear();
		cacheManager.getCache("user").clear();
		
	}
}
