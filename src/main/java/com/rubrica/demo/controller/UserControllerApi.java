package com.rubrica.demo.controller;

import java.util.List;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.rubrica.demo.model.Telephone;
import com.rubrica.demo.model.User;
import com.rubrica.demo.model.UserDetail;
import com.rubrica.demo.repository.TelephoneRepository;
import com.rubrica.demo.repository.UserDetailRepositoryInterface;
import com.rubrica.demo.repository.UserRepositoryInterface;
import com.rubrica.demo.service.UserServiceInterface;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;


@RestController
@RequestMapping("/api")
@CrossOrigin
@Api(value="rubricademo",tags = "Controller per utenti")
public class UserControllerApi {


	@Autowired
	@Qualifier("mainUserService")
	private UserServiceInterface userService;
	
	@Autowired
	private UserDetailRepositoryInterface usereDetailRepository;
	
	@Autowired
	private TelephoneRepository telephoneRepository;
	
	@ApiOperation(
			value ="Ricerca tutti gli utenti",
			notes = "Restituisce tutti gli utenti",
			response = User.class,
			produces = "application/json"
			)
	@ApiResponses(value =
					{
						@ApiResponse(code = 200,message="OK"),	
						@ApiResponse(code = 404,message="Utenti non trovati")
					})
	@RequestMapping(value = "/users", method = RequestMethod.GET)
	public ResponseEntity<Iterable<User>> index()
	{ 
		
		Iterable<User> users = userService.getAll();
		
		
		return new ResponseEntity<Iterable<User>>(users,HttpStatus.OK);
	}
	

	@RequestMapping(value = "/store", method = RequestMethod.POST)
	public ResponseEntity<User> store(@ApiParam("attributi oggetto user") @RequestBody User userRequest)
	{

		User user = userService.store(userRequest);

		UserDetail userDetail = userRequest.getDetail();
		userDetail.setUser(user);
		usereDetailRepository.save(userDetail);
		
		List<Telephone>telephones =  userRequest.getNumbers();
		
		telephones.forEach((Telephone telephone) -> 
		{
			telephone.setUser(userRequest);
			telephoneRepository.save(telephone);
		});

		
		return new ResponseEntity<>(user,HttpStatus.OK);
	}
}
