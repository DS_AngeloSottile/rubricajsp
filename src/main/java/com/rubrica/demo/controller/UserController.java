package com.rubrica.demo.controller;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.MatrixVariable;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import com.rubrica.demo.FileUploadUtil;
import com.rubrica.demo.exception.NotFoundException;
import com.rubrica.demo.model.HelpCenter;
import com.rubrica.demo.model.Telephone;
import com.rubrica.demo.model.User;
import com.rubrica.demo.model.UserDetail;
import com.rubrica.demo.repository.HelpCenterRepositoryInterface;
import com.rubrica.demo.repository.TelephoneRepository;
import com.rubrica.demo.repository.UserDetailRepositoryInterface;
import com.rubrica.demo.service.UserServiceInterface;


@Controller
@RequestMapping("/user")
public class UserController {

	@Autowired
	@Qualifier("mainUserService")
	private UserServiceInterface userService;
	
	@Autowired
	private HelpCenterRepositoryInterface helpCenterRepository; 
	
	@Autowired
	private TelephoneRepository telephoneRepository;
	
	@Autowired
	private UserDetailRepositoryInterface userDetailRepository;
	
	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public String index(Model model, @RequestParam(required = false, name = "qualcosa") String string /*,  @MatrixVariable(pathVar = "parameters") Map< String, List<String> > parameters  */)
	{ 
		System.out.println(string);
		
		
		//List<String> stringa = parameters.get("stringa"); //?stringa=4
		//System.out.println(stringa);
		
		
		Iterable<User> users = userService.getAll();
		int size = (int) StreamSupport.stream(users.spliterator(), false).count();
		
		
		//quindi, se hai una collection devi fare coem sotto se vuoi implementare le lamba
																//skip -> quanti elementi devi saltare, se mette 2 salterà i primi 2 elementi
																//limit -> ne mette 2 per pagina
		//questa parte lasciala così, attivi la lamda			//collect(Collectors.toList()); così ritorni la collezione
		//users = StreamSupport.stream(users.spliterator(), false).skip(0).limit(2).collect(Collectors.toList());
		
		//ne aggiungo altre
		//users = StreamSupport.stream(users.spliterator(), false).filter(user -> user.getId() > 2).collect(Collectors.toList());
		//users = StreamSupport.stream(users.spliterator(), false).filter(user -> user.getEmail().contains("a")).collect(Collectors.toList());
		
		//Optional<HelpCenter> helpCenter = helpCenterRepository.findById(2);
		//List<User> users = helpCenter.get().getUserList();
		//int size = (int) StreamSupport.stream(users.spliterator(), false).count();
		
		model.addAttribute("users", users);
		model.addAttribute("usersCount", size);
		return "user/index";
	}
	
	@RequestMapping(value = "/index/{name}", method = RequestMethod.GET)
	public String searchByName(@PathVariable(name = "name") String name, Model model)
	{ 
		Iterable<User> users = userService.getByName(name);
		
		model.addAttribute("users", users);
		return "user/index";
	}
	
	//http://localhost:8084/user/cerca?name=Angelo1
	@RequestMapping(value = "/cerca", method = RequestMethod.GET)
	public String searchByNameUrl(@RequestParam(name = "name") String name, Model model)
	{ 
		Iterable<User> users = userService.getByName(name);
		
		model.addAttribute("users", users);
		return "user/index";
	}
	
	
	@RequestMapping("/show/{id}")
	public String getById(@PathVariable int id,Model model, HttpServletRequest request)
	{
		boolean fileIsOk = false;
		
		Optional<User> user = userService.getById(id);
		
		if(user.isEmpty())
		{
			throw new ResponseStatusException(HttpStatus.NOT_FOUND,"item not found");
		}

		model.addAttribute("user",user.get());
		return "user/show";
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.POST )
	public RedirectView delete(@ModelAttribute(name = "deleteId") int id)
	{
		boolean deleted = userService.delete(id);
		
		if(!deleted)
		{
			throw new ResponseStatusException(HttpStatus.NOT_FOUND,"error delete item");
		}
		
		return new  RedirectView("/user/index");
	}
	
	@RequestMapping(value = "/edit", method = RequestMethod.POST )
	public String edit(@ModelAttribute(name = "editId") int id,Model model)
	{

		Optional<User> foundUser = userService.getById(id); 
		
		model.addAttribute("userData",foundUser.get());
		return "/user/edit";
	}
	
	@RequestMapping(value="/update", method = RequestMethod.POST)
	public String update(@ModelAttribute("userData") User user, RedirectAttributes attributes)
	{
		Optional<User> foundUser =  userService.update(user.getId(), user);
		
		if(foundUser.isEmpty())
		{
			throw new ResponseStatusException(HttpStatus.NOT_FOUND,"item not found");
		}
		
	    attributes.addFlashAttribute("success","Anime modificato con successo");
		    
		return  "redirect:/user/index";
		
	}
	
	
	
	@RequestMapping(value = "/createNumber", method = RequestMethod.POST )
	public String createNumber(@ModelAttribute(name = "createId") int id,Model model)
	{
		Telephone telephone = new Telephone();
		Optional<User> foundUser = userService.getById(id); 
		
		model.addAttribute("user",foundUser.get());
		model.addAttribute("telephone",telephone);
		return "/user/createNumber";
	}
	
	
	@RequestMapping(value="/storeNumber", method = RequestMethod.POST)
	public String storeNumber (@ModelAttribute("telephone") Telephone telephoneRequest, @RequestParam("userId") int id) 	
	{
		
		Optional<User> foundUser = userService.getById(id);
		
		if(foundUser.isEmpty())
		{
			throw new ResponseStatusException(HttpStatus.NOT_FOUND,"item not found");
		}
		
		Telephone telephone = new Telephone();
		telephone.setNumber(telephoneRequest.getNumber());
		telephone.setUser(foundUser.get());
		
		telephoneRepository.save(telephone);
		
		return "redirect:/user/index";
	}
	
	
	@RequestMapping(value="/create")
	public String create(Model model)
	{ 
		User user = new User();
		
		Iterable<HelpCenter> helpCenters = helpCenterRepository.findAll();
		
		model.addAttribute("userData",user);
		model.addAttribute("helpCenters",getHelpCenters());
		
		return  "/user/create";
	}
	
	
	//REQUEST PARAM PRENDE LA REQUEST DEL FORM, IN QUESTO CASO PHOTO E CITY NON VENGONO MESSI DENTRO USER
	@RequestMapping(value="/store", method = RequestMethod.POST)
	public String store(@Valid @ModelAttribute("userData") User user,BindingResult result, @RequestParam("photo") MultipartFile multipartFile, @RequestParam("city") String city, RedirectAttributes attributes) throws IOException
	{
	
		if(result.hasErrors())
		{
			return "/user/create";
		}
		
		if(result.getSuppressedFields().length > 0)
		{
			throw new RuntimeException("Errore, campi non consentiti" + StringUtils.arrayToCommaDelimitedString(result.getSuppressedFields()));
		}
		
		String fileName = StringUtils.cleanPath(multipartFile.getOriginalFilename());
		user.setImage(fileName);
		
		
		User savedUser = userService.store(user);
		
		UserDetail userDetail = new UserDetail();
		
		userDetail.setCity(city);
		userDetail.setUser(savedUser);
		userDetailRepository.save(userDetail);
		
		String uploadDir = "images/user/" + savedUser.getId();
		
        FileUploadUtil.saveFile(uploadDir, fileName, multipartFile);
        
		attributes.addFlashAttribute("success","Utente creato");
		return "redirect:/user/index";
	}
	
	@InitBinder
	public void initialiseBinder(WebDataBinder binder)
	{
		binder.setAllowedFields(); //se inserisci le colonne vuol dire che solo quelle possono essere modificate
	}
	
	
	@ModelAttribute("helpCenters")
	public Iterable<HelpCenter> getHelpCenters()
	{
		Iterable<HelpCenter> helpCenters = helpCenterRepository.findAll();
		
		return helpCenters;
	}
	
	@ExceptionHandler(NotFoundException.class)
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView handleError(HttpServletRequest request, NotFoundException exception)
	{
		ModelAndView mav = new ModelAndView();
		mav.addObject("codice", exception.getCode());
		mav.addObject("exception",exception);
		
		mav.setViewName("404NotFound");
		
		return mav;
	}
}
