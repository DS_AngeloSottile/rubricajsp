<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
	  
	<h:head>
	
		    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
			<title>Rubrica telefonica</title>
		
	</h:head>
	<h:body>
		<h1> Benvenuto nella tua rubrica, ci sono ${usersCount}</h1>
		
		
		<c:forEach  items="${users}" var="user">

			<h1>Utente: ${user.name}</h1>
			<h1>Dettagli: ${user.detail.age}</h1>
			
			<h1>Help centers</h1>
			<c:forEach items="${user.helpCenters}" var="helpCenter">
				<h3>${helpCenter.name} </h3>
			</c:forEach>
			
			<h1>Telefoni</h1>
			<c:forEach items="${user.numbers}" var="number">
				<h3>${number.number} </h3>
			</c:forEach>
			
			
			<a href=" <spring:url value="/user/show/${user.id}" />" > 
				Dettaglio
			</a>
			

			<form  method="post" action="/user/delete" id=""> 
				<input type="hidden" name="deleteId" value="${user.id}">  
				<button type="submit" id="delete">Elimina</button>
			</form>
				
			<form  method="post" action="/user/edit" id=""> 
				<input type="hidden" name="editId" value="${user.id}">  
				<button type="submit" id="delete">Modifica</button>
			</form>
			
			<form  method="post" action="/user/createNumber" id=""> 
				<input type="hidden" name="createId" value="${user.id}">  
				<button type="submit" id="delete">Aggiungi numero</button>
			</form>
				
				
		</c:forEach>
	</h:body>
	
</html>