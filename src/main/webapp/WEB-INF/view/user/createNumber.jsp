<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
	  
	<h:head>
	
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
		<title>Rubrica telefonica</title>
			
	</h:head>
	<h:body>
	
            <h1>Crea il numero di ${user.name}</h1>

			<form:form  action="storeNumber" method="post" modelAttribute="telephone" > 
			
				<form:errors path="*" cssClass="alert alert-danger" element="div"/>
				
				<input type="hidden" name="userId" value="${user.id}"> 
				<label for="number">number</label>
					<form:input type="text" id="number" path="number" name="number"/>
				<br>
				
				<button type="submit" name="create">Crea contatto servlet</button>
				
			</form:form>
			
	</h:body>

</html>