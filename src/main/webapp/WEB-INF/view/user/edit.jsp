<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
	  
	<h:head>
	
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
		<title>Rubrica telefonica</title>
			
	</h:head>
	<h:body>
		
	
            	<a href="?language=en"><img src="<c:url value="/images/US.png" />"></a> - 
            	<a href="?language=it"><img src="<c:url value="/images/IT.png" />"></a>  
            	
		<h1> MODIFICA UTENTE ${userData.name}</h1>

			<form:form  action="update" method="post" modelAttribute="userData" enctype="multipart/form-data"> 
			<form:errors path="*" cssClass="alert alert-danger" element="div"/>
				
				
				<form:input type="hidden" id="id" path="id" name="id"/>
				
				<label for="nome">Nome</label>
				<form:input type="text" id="name" path="name" name="name"/>
				<form:errors path="name" cssClass="text-danger" />
				<br>
				
				<label for="cognome">Cognome</label>
				<form:input type="text" id="surname" path="surname" name="surname"/>
				<br>
                    
				<label for="email">Età</label>
				<form:input type="text" id="detail.age" path="detail.age" name="detail.age"/>
				<br>
				
				<label for="helpCenters">Help Centers</label>
				<form:select path="helpCenters" >
					<form:options items="${helpCenters}" itemValue="id" itemLabel="name" />
				</form:select>
				<br>
				
				<button type="submit" name="create">MODIFICA</button>
				
			</form:form>
			
	</h:body>

</html>